# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Base de datos: rapp
# Tiempo de Generación: 2017-03-30 04:54:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla battles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `battles`;

CREATE TABLE `battles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` varchar(50) DEFAULT NULL,
  `preview_url` varchar(255) DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `stage_id` int(11) DEFAULT NULL,
  `is_historic` int(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tournament_id` (`tournament_id`),
  KEY `stage_id` (`stage_id`),
  CONSTRAINT `battles_ibfk_1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `battles_ibfk_2` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `battles` WRITE;
/*!40000 ALTER TABLE `battles` DISABLE KEYS */;

INSERT INTO `battles` (`id`, `video_id`, `preview_url`, `likes`, `views`, `tournament_id`, `stage_id`, `is_historic`, `created`, `updated`)
VALUES
	(1,'fcz0jYUfX6o','http://i.imgur.com/R4IhAMN.jpg',25,56,1,3,NULL,'2017-03-22 22:34:57','2017-03-30 03:52:43'),
	(2,'OITRUF96x4o','http://i.imgur.com/axYUVHG.jpg',40,24,3,2,1,'2017-03-22 22:35:01','2017-03-30 03:34:23'),
	(3,'qOz9fgeapI0','http://i.imgur.com/7qVJPUH.jpg',93,89,2,3,NULL,'2017-03-22 22:35:02','2017-03-30 04:40:21');

/*!40000 ALTER TABLE `battles` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla battles_rappers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `battles_rappers`;

CREATE TABLE `battles_rappers` (
  `battle_id` int(11) NOT NULL,
  `rapper_id` int(11) NOT NULL,
  `winner` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`battle_id`,`rapper_id`),
  CONSTRAINT `battles_rappers_ibfk_1` FOREIGN KEY (`battle_id`) REFERENCES `battles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `battles_rappers` WRITE;
/*!40000 ALTER TABLE `battles_rappers` DISABLE KEYS */;

INSERT INTO `battles_rappers` (`battle_id`, `rapper_id`, `winner`)
VALUES
	(1,1,NULL),
	(1,2,1),
	(2,3,NULL),
	(2,4,1),
	(3,5,NULL),
	(3,6,NULL);

/*!40000 ALTER TABLE `battles_rappers` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla rappers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rappers`;

CREATE TABLE `rappers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `birthplace` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `biography` text,
  `is_historic` int(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rappers` WRITE;
/*!40000 ALTER TABLE `rappers` DISABLE KEYS */;

INSERT INTO `rappers` (`id`, `name`, `full_name`, `image_url`, `birthplace`, `birthday`, `biography`, `is_historic`, `created`, `updated`)
VALUES
	(1,'Kodigo','Lucas Gonzalo','http://i.imgur.com/gi298qL.jpg',NULL,NULL,NULL,1,'2017-03-22 22:35:06','2017-03-22 22:35:06'),
	(2,'Tata','TataRap','http://i.imgur.com/vmAjecc.jpg',NULL,NULL,NULL,1,'2017-03-22 22:37:58','2017-03-22 22:37:58'),
	(3,'Aczino',NULL,'http://i.imgur.com/7nJGx72.jpg',NULL,NULL,NULL,NULL,'2017-03-22 22:38:08','2017-03-22 22:38:08'),
	(4,'Chuty',NULL,'http://i.imgur.com/xiOmEjn.jpg',NULL,NULL,NULL,1,'2017-03-22 22:38:17','2017-03-22 22:38:17'),
	(5,'Papo',NULL,'http://i.imgur.com/pLMGeFb.png',NULL,NULL,NULL,NULL,'2017-03-22 22:38:25','2017-03-22 22:38:25'),
	(6,'Shair','Shair Lopez','http://i.imgur.com/JSidJCs.jpg',NULL,NULL,NULL,NULL,'2017-03-22 22:38:33','2017-03-22 22:38:33');

/*!40000 ALTER TABLE `rappers` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla rappers_tournaments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rappers_tournaments`;

CREATE TABLE `rappers_tournaments` (
  `rapper_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`rapper_id`,`tournament_id`),
  CONSTRAINT `rappers_tournaments_ibfk_1` FOREIGN KEY (`rapper_id`) REFERENCES `rappers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `rappers_tournaments` WRITE;
/*!40000 ALTER TABLE `rappers_tournaments` DISABLE KEYS */;

INSERT INTO `rappers_tournaments` (`rapper_id`, `tournament_id`, `position`)
VALUES
	(1,1,1),
	(1,2,1),
	(1,3,4),
	(2,1,3),
	(2,3,2),
	(3,1,2),
	(3,2,4),
	(4,2,2),
	(4,3,5),
	(5,2,5),
	(5,3,3),
	(6,2,3),
	(6,3,1);

/*!40000 ALTER TABLE `rappers_tournaments` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla stages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stages`;

CREATE TABLE `stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `stages` WRITE;
/*!40000 ALTER TABLE `stages` DISABLE KEYS */;

INSERT INTO `stages` (`id`, `name`)
VALUES
	(1,'Octavos de Final'),
	(2,'Cuartos de Final'),
	(3,'Semifinal'),
	(4,'Final');

/*!40000 ALTER TABLE `stages` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla tournaments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tournaments`;

CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `logo_url` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `jury` varchar(255) DEFAULT NULL,
  `likes` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `is_historic` int(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tournaments` WRITE;
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;

INSERT INTO `tournaments` (`id`, `title`, `date`, `logo_url`, `place`, `host`, `jury`, `likes`, `views`, `is_historic`, `created`, `updated`)
VALUES
	(1,'Red Bull Internacional 2016','2016-09-19','http://i.imgur.com/rBXsn91.png','Espacio Nectar','Misionero','Dtoke',2,2,NULL,'2017-03-22 20:43:57','2017-03-30 03:53:45'),
	(2,'Exhibición en Misiones','2016-05-09','http://i.imgur.com/rBXsn91.png','Espacio Nectar','Misionero','Kodigo',9,3,NULL,'2017-03-22 20:45:06','2017-03-22 22:36:30'),
	(3,'BDM Deluxe - 2016','2016-02-05','http://i.imgur.com/rBXsn91.png','Espacio Nectar','Misionero',NULL,20,9,NULL,'2017-03-22 20:45:03','2017-03-22 22:36:30');

/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
