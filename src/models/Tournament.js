const { bookshelf, knex, model } = require('../bin')

require('./Battle')
require('./Rapper')

const maxRappersByTournament = 5

const reduceTournamentToRappers = (tournament) => {
  let rappersIds = []

  return tournament.toJSON().battles
    .reduce((tournamentRappers, battle) => {
      const battleRapper = battle.rappers.filter(rapper => rappersIds.indexOf(rapper.id) === -1)

      rappersIds = rappersIds.concat(battle.rappers.map(rapper => rapper.id))

      return tournamentRappers.concat(battleRapper)
    }, [])
    .slice(0, maxRappersByTournament)
}

const Tournament = model.extend({
  tableName: 'tournaments',
  hidden: ['metadata'],
  battles() {
    return this.hasMany('Battle', 'tournament_id')
  },
  scopes: {
    latest: qb => qb.orderBy('created', 'DESC').orderBy('id', 'DESC'),
    historic: qb => qb.where({ is_historic: true }).orderBy('created', 'DESC').orderBy('id', 'DESC'),
    popular: qb => qb.orderBy('views', 'DESC').orderBy('created', 'DESC').orderBy('id', 'DESC'),
  },
}, {
  searchableFields: [
    'title',
    'place',
    'host',
    'jury',
  ],
  allWithRappers(options) {
    return this
      .all(Object.assign({}, options, {
        relations: [
          {
            battles: qb => {
              qb.column('id', 'tournament_id')
              qb.where('stage_id', '<', 3)
              qb.orderBy('stage_id', 'ASC')
            },
          }, {
            'battles.rappers': qb => {
              qb.column('id', 'name', 'image_url')
            },
          },
        ],
      })).then(tournaments => tournaments.map(originalTournament => {
        const tournament = originalTournament

        tournament.attributes.rappers = reduceTournamentToRappers(tournament)

        delete tournament.relations.battles

        return tournament
      }))
  },
  async getByRapperId(rapperId) {
    const [rawTournaments] = await knex.raw(`
      SELECT     b.tournament_id
      FROM       battles b
      INNER JOIN battles_rappers br ON br.battle_id = b.id and br.rapper_id = ${rapperId}
      GROUP BY   b.tournament_id
    `)

    const tournamentsIds = rawTournaments.map(rawTournament => rawTournament.tournament_id)

    return this
      .all({
        qb: qb => qb.where('id', 'in', tournamentsIds).orderBy('date', 'DESC'),
        relations: [
          {
            battles: qb => {
              qb.column('id', 'tournament_id', 'stage_id')
              qb.orderBy('stage_id', 'ASC')
            },
          }, {
            'battles.rappers': qb => {
              qb.column('id', 'name', 'image_url')
            },
          },
        ],
      }).then(tournaments => tournaments.map(originalTournament => {
        const tournament = originalTournament

        tournament.attributes.rappers = reduceTournamentToRappers(tournament)

        delete tournament.relations.battles

        return tournament
      }))
  },
  create(fields) {
    const { title, logoUrl, date, place, host, jury, isHistoric, migrated, metadata } = fields

    return new Tournament({
      title,
      logoUrl,
      date,
      place,
      host,
      jury,
      isHistoric,
      migrated,
      metadata,
      created: new Date(),
      updated: new Date(),
    })
      .save()
  },
})

module.exports = bookshelf.model('Tournament', Tournament)
