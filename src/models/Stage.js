const { bookshelf, model } = require('../bin')

const Stage = model.extend({
  tableName: 'stages',
})

module.exports = bookshelf.model('Stage', Stage)
