const { bookshelf, model } = require('../bin')

require('./Battle')
require('./Tournament')

const Rapper = model.extend({
  tableName: 'rappers',
  battles() {
    return this.belongsToMany('Battle', 'battles_rappers')
  },
  scopes: {
    latest: qb => qb.orderBy('created', 'DESC').orderBy('id', 'DESC'),
    historic: qb => qb.where({ is_historic: true }).orderBy('views', 'DESC').orderBy('created', 'DESC').orderBy('id', 'DESC'),
    popular: qb => qb.orderBy('views', 'DESC').orderBy('created', 'DESC').orderBy('id', 'DESC'),
  },
}, {
  searchableFields: [
    'name',
    'full_name',
    'birthplace',
  ],
  create(fields) {
    const {
      name,
      fullName,
      imageUrl,
      birthday,
      birthplace,
      biography,
      isHistoric,
      instagram,
      twitter,
      youtube,
    } = fields

    return new Rapper({
      name,
      fullName,
      imageUrl,
      birthday,
      birthplace,
      biography,
      isHistoric,
      instagram,
      twitter,
      youtube,
      created: new Date(),
      updated: new Date(),
    })
      .save()
  },
})

module.exports = bookshelf.model('Rapper', Rapper)
