const { knex, bookshelf, model } = require('../bin')
const { getPagedRows } = require('../util/rows')
const { getIdFromUrl } = require('../util/youtube')

require('./Stage')
require('./Tournament')
const Rapper = require('./Rapper')

const Battle = model.extend({
  tableName: 'battles',
  tournament() {
    return this.belongsTo('Tournament')
  },
  stage() {
    return this.belongsTo('Stage')
  },
  rappers() {
    return this.belongsToMany('Rapper', 'battles_rappers').withPivot('team')
  },
  scopes: {
    latest: qb => qb.orderBy('created', 'DESC').orderBy('id', 'DESC'),
    historic: qb => qb.whereNotNull('video_id').where({ is_historic: true }).orderBy('created', 'DESC').orderBy('id', 'DESC'),
    popular: qb => qb.whereNotNull('video_id').orderBy('views', 'DESC').orderBy('created', 'DESC').orderBy('id', 'DESC'),
  },
}, {
  relations: {
    tournament: qb => qb.column('id', 'title'),
    stage: qb => qb.column('id', 'name'),
    rappers: qb => qb.column('id', 'name', 'image_url', 'team'),
  },
  searchableFields: [
    'video_id',
  ],
  async search({ query, page }) {
    if (query.includes('vs')) {
      const separator = query.includes('vs.') ? 'vs.' : 'vs'

      const rappersNames = query.split(separator).map(name => name.trim())

      const rappers = await Rapper.all({
        columns: ['id'],
        qb: qb => rappersNames.map(name => qb.orWhere('name', 'LIKE', `%${name}%`)),
      })

      const [firstRapperId, secondRapperId] = rappers.map(rapper => rapper.id)

      if (!firstRapperId || !secondRapperId) {
        return []
      }

      const [rawBattles] = await knex.raw(`
        SELECT   battle_id
        FROM     battles_rappers
        WHERE    rapper_id IN (${firstRapperId}, ${secondRapperId})
        GROUP BY battle_id
        HAVING   count(*) = 2
      `)

      const battlesIds = rawBattles.map(rawBattleId => rawBattleId.battle_id)

      const battles = await Battle.all({
        qb: qb => qb.where('id', 'in', battlesIds),
      })

      return getPagedRows(battles, page)
    }

    const rappers = await Rapper.all({
      query,
      relations: {
        'battles.rappers': this.relations.rappers,
        'battles.stage': this.relations.stage,
        'battles.tournament': this.relations.tournament,
      },
    })

    const battles = rappers.reduce((accumulator, rapper) => [
      ...accumulator,
      ...rapper.related('battles').serialize(),
    ], [])

    return getPagedRows(battles, page)
  },
  getRappersByTournamentId(tournamentId) {
    /*
    SELECT      br.rapper_id
    FROM        battles b
    INNER JOIN  battles_rappers br ON br.battle_id = b.id
    WHERE       b.tournament_id = 9
    GROUP BY    br.rapper_id, b.stage_id
    ORDER BY    b.stage_id ASC
    */

    return this
      .query(qb => {
        qb.where({ tournament_id: tournamentId })
        qb.orderBy('stage_id', 'ASC')
      })
      .fetchAll({
        withRelated: 'rappers',
      })
      .then(battles => {
        const rappersIds = []

        return battles.reduce((tournamentRappers, battle) => tournamentRappers
          .concat(battle.related('rappers').toJSON().map(originalRapper => {
            if (!rappersIds.includes(originalRapper.id)) {
              const rapper = originalRapper

              rappersIds.push(rapper.id)

              rapper.position = battle.attributes.stageId * 2

              if (battle.attributes.winnerTeam === rapper._pivot_team) rapper.position -= 1 // eslint-disable-line no-underscore-dangle,max-len

              return rapper
            }

            return { id: -1 }
          })
          .sort((a, b) => a.position - b.position)),
        [])
        .filter(item => item.id !== -1)
        .map((item, itemIndex) => ({
          ...item,
          position: itemIndex,
        }))
      })
  },
  create(fields) {
    const { tournamentId, stageId, winnerTeam, videoUrl, previewUrl, isHistoric, teams } = fields

    const battleRappers = []

    if (teams.length > 0) {
      teams.slice(1).map((rappers, team) => rappers.map(rapperId => battleRappers.push({
        rapper_id: rapperId,
        team,
      })))
    }

    return new Battle({
      tournamentId,
      stageId,
      winnerTeam,
      isHistoric,
      videoId: videoUrl && getIdFromUrl(videoUrl),
      previewUrl,
      created: new Date(),
      updated: new Date(),
    })
      .save()
      .then(battle => {
        battle.rappers().attach(battleRappers)
      })
  },
})

module.exports = bookshelf.model('Battle', Battle)
