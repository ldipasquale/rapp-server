const url = require('url')

const dbConfig = url.parse(process.env.CLEARDB_DATABASE_URL)
const [dbUser, dbPassword] = dbConfig.auth.split(':')

module.exports = {
  HOST: dbConfig.host,
  USER: dbUser,
  PASS: dbPassword,
  DATABASE: dbConfig.pathname.substring(1),
}
