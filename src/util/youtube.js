module.exports = {
  getIdFromUrl(url) {
    const videoId = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/)

    if (videoId !== null) {
      return videoId[1]
    }

    return false
  },
}
