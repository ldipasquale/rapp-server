const { PAGE_SIZE } = require('../config/general')

module.exports = {
  getPagedRows(rows, page) {
    if (!page || page === 'undefined') {
      return rows
    }

    const firstRow = (page - 1) * PAGE_SIZE
    const lastRow = (page) * PAGE_SIZE

    return rows.slice(firstRow, lastRow)
  },
}
