const { bookshelf } = require('./db')
const generalConfig = require('../config/general')

module.exports = bookshelf.Model.extend({}, {
  orderBy(originalField) {
    let field = originalField
    let order = 'ASC'

    if (field[0] === '-') {
      order = 'DESC'
      field = field.substr(1)
    }

    return this.query(qb => qb.orderBy(field, order))
  },
  get(id, options) {
    const args = {}

    // Set relationship in query
    if (options && options.relations) {
      args.withRelated = options.relations
    } else if (this.relations) {
      args.withRelated = this.relations
    }

    return this.where({ id }).fetch(args)
  },
  all(options) {
    let model = this

    const args = {}

    // Set relationship in query
    if (options && options.relations) {
      args.withRelated = options.relations
    } else if (this.relations) {
      args.withRelated = this.relations
    }

    if (options) {
      if (options.scope && typeof model[options.scope] !== undefined) {
        model = model[options.scope]()
      }

      // Set condition in query
      if (this.searchableFields && options.query) {
        model = model.query(qb => this.searchableFields
          .map(field => qb.orWhere(field, 'LIKE', `%${options.query}%`))
        )
      }

      if (options.qb) {
        model = model.query(options.qb)
      }

      if (options.columns) {
        args.columns = options.columns
      }

      if (options.page === -1) {
        return model.fetchAll(args)
      }
    }

    args.page = options.page || 1
    args.pageSize = generalConfig.PAGE_SIZE

    return model.fetchPage(args)
  },
})
