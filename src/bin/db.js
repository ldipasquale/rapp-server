const config = require('../config/database')

const knex = require('knex')({
  // debug: true,
  client: 'mysql',
  connection: {
    host: config.HOST,
    user: config.USER,
    password: config.PASS,
    database: config.DATABASE,
    port: config.PORT,
    charset: 'utf8',
  },
})

const bookshelf = require('bookshelf')(knex)

bookshelf.plugin([
  'registry',
  'bookshelf-camelcase',
  'bookshelf-scopes',
  'pagination',
  'visibility',
])

module.exports = {
  knex,
  bookshelf,
}
