const { knex, bookshelf } = require('./db')
const model = require('./model')

module.exports = {
  knex,
  bookshelf,
  model,
}
