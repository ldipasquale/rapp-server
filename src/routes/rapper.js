const router = require('express').Router()
const { getPagedRows } = require('../util/rows')
const { Tournament, Rapper, Battle } = require('../models/')

router.get('/', (req, res) => {
  if (req.query.q) {
    return Rapper
      .all({
        scope: 'latest',
        query: req.query.q,
        page: req.query.page,
      })
      .then(rappers => res.status(200).send(rappers))
  }

  return Rapper
    .all({
      scope: 'latest',
      page: req.query.page,
    })
    .then(rappers => res.status(200).send(rappers))
})

router.post('/', (req, res) => Rapper
  .create(req.body)
  .then(() => res.status(200).send())
  .catch(error => res.status(400).send(error))
)

router.get('/popular', (req, res) => Rapper
  .all({
    scope: 'popular',
    page: req.query.page,
  })
  .then(rappers => res.status(200).send(rappers))
)

router.get('/historic', (req, res) => Rapper
  .all({
    scope: 'historic',
    page: req.query.page,
  })
  .then(rappers => res.status(200).send(rappers))
)

router.get('/:id', (req, res) => Rapper
  .query()
  .where('id', req.params.id)
  .increment('views', 1)
  .then(() => Rapper
    .get(req.params.id)
    .then(rapper => res.status(200).send(rapper))
  )
)

router.delete('/:id', (req, res) => new Rapper({ id: req.params.id })
  .destroy()
  .then(() => res.status(200).send())
  .catch(error => res.status(400).send(error))
)

router.get('/:id/battles', (req, res) => Rapper
  .get(req.params.id, {
    relations: {
      battles: qb => qb.orderBy('created', 'DESC'),
      'battles.tournament': Battle.relations.tournament,
      'battles.stage': Battle.relations.stage,
      'battles.rappers': Battle.relations.rappers,
    },
  })
  .then(rapper => res.status(200).send(rapper && getPagedRows(rapper.related('battles'), req.query.page)))
)

router.get('/:id/tournaments', (req, res) => Tournament
  .getByRapperId(req.params.id)
  .then(tournaments => res.status(200).send(getPagedRows(tournaments, req.query.page)))
)

router.patch('/:id', (req, res) => {
  if (req.body.likes === '+1') {
    return Rapper
      .query()
      .where('id', req.params.id)
      .increment('likes', 1)
      .then(() => res.status(200).send(true))
  }

  return res.status(400)
})

module.exports = router
