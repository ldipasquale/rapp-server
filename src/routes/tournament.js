const router = require('express').Router()
const { getPagedRows } = require('../util/rows')
const { Battle, Tournament } = require('../models/')

router.get('/', (req, res) => {
  if (req.query.q) {
    return Tournament
      .allWithRappers({
        scope: 'latest',
        query: req.query.q,
        page: req.query.page,
      })
      .then(tournaments => res.status(200).send(tournaments))
  }

  return Tournament
    .allWithRappers({
      scope: 'latest',
      page: req.query.page,
    })
    .then(tournaments => res.status(200).send(tournaments))
})

router.post('/', (req, res) => Tournament
  .create(req.body)
  .then(data => res.status(200).send(data))
  .catch(error => res.status(400).send(error))
)

router.get('/popular', (req, res) => Tournament
  .allWithRappers({
    scope: 'popular',
    page: req.query.page,
  })
  .then(tournaments => res.status(200).send(tournaments))
)

router.get('/historic', (req, res) => Tournament
  .allWithRappers({
    scope: 'historic',
    page: req.query.page,
  })
  .then(tournaments => res.status(200).send(tournaments))
)

router.get('/:id', (req, res) => Tournament
  .query()
  .where('id', req.params.id)
  .increment('views', 1)
  .then(() => Tournament
    .get(req.params.id)
    .then(tournament => res.status(200).send(tournament))
  )
)

router.get('/:id/battles', (req, res) => Tournament
  .get(req.params.id, {
    relations: {
      battles: qb => qb.orderBy('stage_id', 'DESC'),
      'battles.tournament': Battle.relations.tournament,
      'battles.stage': Battle.relations.stage,
      'battles.rappers': Battle.relations.rappers,
    },
  })
  .then(tournament => res.status(200).send(tournament && getPagedRows(tournament.related('battles'), req.query.page)))
)

router.get('/:id/rappers', (req, res) => Battle
  .getRappersByTournamentId(req.params.id)
  .then(rappers => res.status(200).send(getPagedRows(rappers, req.query.page)))
)

router.patch('/:id', (req, res) => {
  if (req.body.likes === '+1') {
    return Tournament
      .query()
      .where('id', req.params.id)
      .increment('likes', 1)
      .then(() => res.status(200).send(true))
  }

  return res.status(400)
})

router.delete('/:id', (req, res) => Tournament.forge({ id: req.params.id })
  .destroy()
  .then(() => res.status(200).send())
  .catch(error => res.status(400).send(error))
)

module.exports = router
