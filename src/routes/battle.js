const router = require('express').Router()
const { Battle } = require('../models/')

router.get('/', (req, res) => {
  if (req.query.q) {
    return Battle
      .search({
        query: req.query.q,
        page: req.query.page,
      })
      .then(battles => res.status(200).send(battles))
  }

  return Battle
    .all({
      scope: 'latest',
      page: req.query.page,
    })
    .then(battles => res.status(200).send(battles))
})

router.post('/', (req, res) => Battle
  .create(req.body)
  .then(() => res.status(200).send())
  .catch(error => res.status(400).send(error))
)

router.get('/popular', (req, res) => Battle
  .all({
    scope: 'popular',
    page: req.query.page,
  })
  .then(battles => res.status(200).send(battles))
)

router.get('/historic', (req, res) => Battle
  .all({
    scope: 'historic',
    page: req.query.page,
  })
  .then(battles => res.status(200).send(battles))
)

router.get('/:id', (req, res) => Battle
  .query()
  .where('id', req.params.id)
  .increment('views', 1)
  .then(() => Battle
    .get(req.params.id)
    .then(battle => res.status(200).send(battle))
  )
)

router.patch('/:id', (req, res) => {
  if (req.body.likes === '+1') {
    return Battle
      .query()
      .where('id', req.params.id)
      .increment('likes', 1)
      .then(() => res.status(200).send(true))
  }

  return res.status(400)
})

router.delete('/:id', (req, res) => new Battle({ id: req.params.id })
  .destroy()
  .then(() => res.status(200).send())
  .catch(error => res.status(400).send(error))
)

module.exports = router
