const router = require('express').Router()
const { Stage } = require('../models/')

router.get('/', (req, res) => Stage
  .all()
  .then(stages => res.status(200).send(stages))
)

module.exports = router
