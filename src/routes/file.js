const router = require('express').Router()
const aws = require('aws-sdk')
const uuidv4 = require('uuid/v4')

const awsConfig = require('../config/aws')

router.get('/sign', (req, res) => {
  if ((req.query.fileSize / 1024) > 500) {
    return res.status(400).send('File size')
  }

  aws.config.update({
    accessKeyId: awsConfig.AWS_ACCESS_KEY,
    secretAccessKey: awsConfig.AWS_SECRET_KEY,
  })

  const extension = req.query.fileName.split('.').slice(1).join('.')
  const hash = uuidv4()

  const fileName = `${hash}.${extension}`

  const s3 = new aws.S3()

  const options = {
    Bucket: awsConfig.S3_BUCKET,
    Key: fileName,
    Expires: 60,
    ContentType: req.query.fileType,
    ACL: 'public-read',
  }

  return s3.getSignedUrl('putObject', options, (err, data) => {
    if (err) return res.status(400).send('Error with S3')

    return res.status(200).json({
      signedUrl: data,
      publicUrl: `https://s3.amazonaws.com/${awsConfig.S3_BUCKET}/${fileName}`,
    })
  })
})

module.exports = router
