const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(methodOverride())

app.use((req, res, next) => {
  const origin = req.get('origin')

  res.header('Access-Control-Allow-Origin', origin)
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control, Pragma') // eslint-disable-line max-len

  if (req.method === 'OPTIONS') {
    res.sendStatus(204)
  } else {
    next()
  }
})

app.get('/', (req, res) => res.status(200).send('ok'))

app.use('/battle', require('./routes/battle'))
app.use('/rapper', require('./routes/rapper'))
app.use('/tournament', require('./routes/tournament'))
app.use('/stage', require('./routes/stage'))
app.use('/file', require('./routes/file'))

app.listen(process.env.PORT || 3000, () => console.log('Node server running on http://localhost:3000')) // eslint-disable-line no-console,max-len
